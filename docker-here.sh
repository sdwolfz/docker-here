#!/usr/bin/env sh

dryrun=0
user=''
workdir='/here'

while [ $# -gt 0 ]
do
  key="$1"
  case "$key" in
    -r)
      user="0:0"
      shift
      ;;
    -w)
      workdir="$2"
      shift
      shift
      ;;
    -V)
      dryrun=1
      shift
      ;;
    *)
      break
      ;;
  esac
done

if [ -z $user ]
then
  user="$(id -u):$(id -g)"
fi

command="docker run --rm -it -u $user -v $PWD:$workdir -w $workdir $*"

if [ $dryrun -eq 0 ]
then
  # shellcheck disable=SC2086
  exec $command
else
  printf "Warning: This prints out the values of ENV variables part of the " >&2
  printf "command, potentially exposing passwords to whoever is watching "   >&2
  printf "your screen. \n\n" >&2

  echo "$command"
fi
