#-------------------------------------------------------------------------------
# CI
#-------------------------------------------------------------------------------

HERE := $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v "$(HERE)":/dot -w /dot

.PHONY: ci
ci: ci/eclint ci/shellcheck ci/yamllint

.PHONY: ci/eclint
ci/eclint:
	@$(DOCKER_HERE) sdwolfz/eclint:latest make lint/eclint

.PHONY: ci/shellcheck
ci/shellcheck:
	@$(DOCKER_HERE) sdwolfz/shellcheck:latest make lint/shellcheck

.PHONY: ci/yamllint
ci/yamllint:
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

.PHONY: lint
lint: lint/eclint lint/shellcheck lint/yamllint

.PHONY: lint/eclint
lint/eclint:
	@eclint check $$(git ls-files)

.PHONY: lint/shellcheck
lint/shellcheck:
	@shellcheck ./docker-here.sh

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint .

#-------------------------------------------------------------------------------
# Install
#-------------------------------------------------------------------------------

.PHONY: install
install:
	@\cp -f ./docker-here.sh /usr/local/bin/docker-here
	@chmod +x /usr/local/bin/docker-here
