# Docker Here

Execute `docker run` the right way!

> **NOTE**: This project is feature complete! there will be no changes to it's
> API, unless the `docker/cli` API changes.

**Table of Contents**

- [Docker Here](#docker-here)
    - [What?](#what)
    - [Bash alias](#bash-alias)
    - [Options](#options)
    - [Install](#install)
    - [LICENSE](#license)

## What?

This is too long to type:

```sh
docker run --rm -it -u "$(id -u)":"$(id -g)" -v "$PWD":/here -w /here alpine ls
```

So type this instead:

```sh
docker-here alpine ls
```

## Bash alias

You could add this in your `~/.bash_profile`:

```bash
alias docker-here='docker run --rm -it -u "$(id -u)":"$(id -g)" -v "$PWD":/here -w /here'
```

But this does not work in all cases, like when using `envchain`, or anything
else that tries to execute a command without a login shell.

## Options

Use `-r` to run the container as root.

```sh
docker-here -r alpine whoami

# Same as executing:
# docker run --rm -it -u 0:0 -v /home/user:/here -w /here alpine whoami
```

Run with the `-w` flag to change the workspace mount and location.

```sh
docker-here -w /work alpine pwd

# Same as executing:
# docker run --rm -it -u 1000:1000 -v /home/user:/work -w /work alpine pwd
```

To do a dry-run and only print the command without executing, use `-V`.

```sh
docker-here -V alpine pwd

# Prints out:
# docker run --rm -it -u 1000:1000 -v /home/user:/here -w /here alpine pwd
```

## Install

```sh
git clone https://gitlab.com/sdwolfz/docker-here.git
cd docker-here
sudo make install
```

## LICENSE

This repository is covered by the BSD license, see [LICENSE](LICENSE) for
details.
